"""
LittleWorld Project's frontend_part
"""
import os
import json
from flask import Flask, render_template, jsonify
import requests

@app.route('/')
def index():
    module = request.args.get("module")
    exec("import urllib%s as urllib" % int(module))

def foo(a):
    b = 12
    if a == 1:
        return b

target = -5
num = 3
  
target += num 

if __name__ == "__main__":
    app.run(host='0.0.0.0', port=5000)
