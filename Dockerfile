FROM ubuntu AS html_builder

RUN echo "Let's say it's something important written here" > index.html
ARG PORT=3000
ENV ENV_PORT=$PORT
FROM nginx AS runtime

COPY --from=html_builder index.html /usr/share/nginx/html/index.html

EXPOSE $PORT
